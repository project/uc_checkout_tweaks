
$(function() {
  if (Drupal.settings.uc_checkout_tweaks.same_address) {
    if ($('#edit-panes-billing-copy-address').is(':checked')) {
      uc_cart_copy_address(true, 'delivery', 'billing');
    }
    if ($('#edit-panes-delivery-copy-address').is(':checked')) {
      uc_cart_copy_address(true, 'billing', 'delivery');
    }
  }
});
